package com.gsafety.devops.service.impl;

import com.gsafety.devops.entity.SqlListEntity;
import com.gsafety.devops.entity.SqlTaskEntity;
import com.gsafety.devops.service.SqlListServiceI;

import org.jeecgframework.core.common.service.impl.CommonServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import org.jeecgframework.core.common.exception.BusinessException;
import org.jeecgframework.core.common.service.impl.CommonServiceImpl;
import org.jeecgframework.core.util.MyBeanUtils;
import org.jeecgframework.core.util.StringUtil;
import org.jeecgframework.core.util.oConvertUtils;
import java.util.ArrayList;
import java.util.UUID;
import java.io.Serializable;
import org.jeecgframework.core.util.ApplicationContextUtil;
import org.jeecgframework.core.util.MyClassLoader;
import org.jeecgframework.web.cgform.enhance.CgformEnhanceJavaInter;

import java.util.Map;
import java.util.HashMap;
import org.jeecgframework.minidao.util.FreemarkerParseFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.jeecgframework.core.util.ResourceUtil;

@Service("sqlListService")
@Transactional
public class SqlListServiceImpl extends CommonServiceImpl implements SqlListServiceI {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
 	public void delete(SqlListEntity entity) throws Exception{
 		super.delete(entity);
 	}
	public void addMain(SqlListEntity sqlList,
	        List<SqlTaskEntity> sqlTaskList) throws Exception{
	
			//保存主信息
			this.save(sqlList);
		
			/**保存-执行任务*/
			for(SqlTaskEntity sqlTask:sqlTaskList){
				//外键设置
				sqlTask.setSqlId(sqlList.getId());
				this.save(sqlTask);
			}
	}
	
	public void addSqlListEntity(SqlListEntity sqlList) throws Exception{
			//保存信息
			super.save(sqlList);
	}
	public void updateMain(SqlListEntity sqlList,
	        List<SqlTaskEntity> sqlTaskList) throws Exception {
		//保存主表信息
		if(StringUtil.isNotEmpty(sqlList.getId())){
			try {
				SqlListEntity temp = findUniqueByProperty(SqlListEntity.class, "id", sqlList.getId());
				MyBeanUtils.copyBeanNotNull2Bean(sqlList, temp);
				this.saveOrUpdate(temp);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			this.saveOrUpdate(sqlList);
		}
		//===================================================================================
		//获取参数
		Object id0 = sqlList.getId();
		//===================================================================================
		//1.查询出数据库的明细数据-执行任务
	    String hql0 = "from SqlTaskEntity where 1 = 1 AND sqlId = ? ";
	    List<SqlTaskEntity> sqlTaskOldList = this.findHql(hql0,id0);
		//2.筛选更新明细数据-执行任务
		if(sqlTaskList!=null&&sqlTaskList.size()>0){
		for(SqlTaskEntity oldE:sqlTaskOldList){
			boolean isUpdate = false;
				for(SqlTaskEntity sendE:sqlTaskList){
					//需要更新的明细数据-执行任务
					if(oldE.getId().equals(sendE.getId())){
		    			try {
							MyBeanUtils.copyBeanNotNull2Bean(sendE,oldE);
							this.saveOrUpdate(oldE);
						} catch (Exception e) {
							e.printStackTrace();
							throw new BusinessException(e.getMessage());
						}
						isUpdate= true;
		    			break;
		    		}
		    	}
	    		if(!isUpdate){
		    		//如果数据库存在的明细，前台没有传递过来则是删除-执行任务
		    		super.delete(oldE);
	    		}
	    		
			}
			//3.持久化新增的数据-执行任务
			for(SqlTaskEntity sqlTask:sqlTaskList){
				if(oConvertUtils.isEmpty(sqlTask.getId())){
					//外键设置
					sqlTask.setSqlId(sqlList.getId());
					this.save(sqlTask);
				}
			}
		}
		//执行更新操作配置的sql增强
 		//this.doUpdateSql(sqlList);
	}
	
	public void updateSqlListEntity(SqlListEntity sqlList) throws Exception {
		//保存主表信息
		super.saveOrUpdate(sqlList);
	}
	public void delMain(SqlListEntity sqlList) throws Exception {
		//删除主表信息
		this.delete(sqlList);
		//===================================================================================
		//获取参数
		Object id0 = sqlList.getId();
		//===================================================================================
		//删除-执行任务
	    String hql0 = "from SqlTaskEntity where 1 = 1 AND sqlId = ? ";
	    List<SqlTaskEntity> sqlTaskOldList = this.findHql(hql0,id0);
		this.deleteAllEntitie(sqlTaskOldList);
	}
}
<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>端口扫描</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<t:base type="bootstrap,layer,validform,bootstrap-form"></t:base>
</head>
 <body style="overflow:hidden;overflow-y:auto;">
 <div class="container" style="width:100%;">
	<div class="panel-heading"></div>
	<div class="panel-body">
	<form class="form-horizontal" role="form" id="formobj" action="portScanController.do?doUpdate" method="POST">
		<input type="hidden" id="btn_sub" class="btn_sub"/>
		<input type="hidden" id="id" name="id" value="${portScan.id}"/>
	<div class="form-group">
		<label for="portDesc" class="col-sm-3 control-label">端口说明：</label>
		<div class="col-sm-7">
			<div class="input-group" style="width:100%">
				<input id="portDesc" name="portDesc" value='${portScan.portDesc}' type="text" maxlength="50" class="form-control input-sm" placeholder="请输入端口说明"  ignore="ignore" />
			</div>
		</div>
	</div>
	<div class="form-group">
		<label for="ip" class="col-sm-3 control-label">IP地址：</label>
		<div class="col-sm-7">
			<div class="input-group" style="width:100%">
				<input id="ip" name="ip" value='${portScan.ip}' type="text" maxlength="32" class="form-control input-sm" placeholder="请输入IP地址"  ignore="ignore" />
			</div>
		</div>
	</div>
	<div class="form-group">
		<label for="port" class="col-sm-3 control-label">端口：</label>
		<div class="col-sm-7">
			<div class="input-group" style="width:100%">
				<input id="port" name="port" value='${portScan.port}' type="text" maxlength="5" class="form-control input-sm" placeholder="请输入端口"  ignore="ignore" />
			</div>
		</div>
	</div>
	<div class="form-group">
		<label for="scanInterval" class="col-sm-3 control-label">扫描间隔：</label>
		<div class="col-sm-7">
			<div class="input-group" style="width:100%">
				<input id="scanInterval" name="scanInterval" value='${portScan.scanInterval}' type="text" maxlength="32" class="form-control input-sm" placeholder="请输入扫描间隔"  ignore="ignore" />
			</div>
		</div>
	</div>
	<div class="form-group">
		<label for="scanDate" class="col-sm-3 control-label">最后扫描时间：</label>
		<div class="col-sm-7">
			<div class="input-group" style="width:100%">
      		    <input id="scanDate" name="scanDate" type="text" class="form-control input-sm laydate-datetime" placeholder="请输入最后扫描时间"  ignore="ignore"   value="<fmt:formatDate pattern='yyyy-MM-dd HH:mm:ss' type='both' value='${portScan.scanDate}'/>" />
                <span class="input-group-addon" ><span class="glyphicon glyphicon-calendar"></span> </span>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label for="status" class="col-sm-3 control-label">端口状态：</label>
		<div class="col-sm-7">
			<div class="input-group" style="width:100%">
				<input id="status" name="status" value='${portScan.status}' type="text" maxlength="32" class="form-control input-sm" placeholder="请输入端口状态"  ignore="ignore" />
			</div>
		</div>
	</div>
	</form>
	</div>
 </div>
<script type="text/javascript">
var subDlgIndex = '';
$(document).ready(function() {
	//单选框/多选框初始化
	$('.i-checks').iCheck({
		labelHover : false,
		cursor : true,
		checkboxClass : 'icheckbox_square-green',
		radioClass : 'iradio_square-green',
		increaseArea : '20%'
	});
	
	//表单提交
	$("#formobj").Validform({
		tiptype:function(msg,o,cssctl){
			if(o.type==3){
				validationMessage(o.obj,msg);
			}else{
				removeMessage(o.obj);
			}
		},
		btnSubmit : "#btn_sub",
		btnReset : "#btn_reset",
		ajaxPost : true,
		beforeSubmit : function(curform) {
		},
		usePlugin : {
			passwordstrength : {
				minLen : 6,
				maxLen : 18,
				trigger : function(obj, error) {
					if (error) {
						obj.parent().next().find(".Validform_checktip").show();
						obj.find(".passwordStrength").hide();
					} else {
						$(".passwordStrength").show();
						obj.parent().next().find(".Validform_checktip").hide();
					}
				}
			}
		},
		callback : function(data) {
			var win = frameElement.api.opener;
			if (data.success == true) {
				frameElement.api.close();
			    win.reloadTable();
			    win.tip(data.msg);
			} else {
			    if (data.responseText == '' || data.responseText == undefined) {
			        $.messager.alert('错误', data.msg);
			        $.Hidemsg();
			    } else {
			        try {
			            var emsg = data.responseText.substring(data.responseText.indexOf('错误描述'), data.responseText.indexOf('错误信息'));
			            $.messager.alert('错误', emsg);
			            $.Hidemsg();
			        } catch (ex) {
			            $.messager.alert('错误', data.responseText + "");
			            $.Hidemsg();
			        }
			    }
			    return false;
			}
		}
	});
});
</script>
</body>
</html>